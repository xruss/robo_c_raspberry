#include "consoleprinter.h"
#include <iostream>
#include <iomanip>

using namespace std;

ConsolePrinter::ConsolePrinter(QObject *parent) : QObject(parent)
{

}


void ConsolePrinter::printDistance(double dist){
    cout<<"measured distance: "<<std::setw(6)<<std::setprecision(4) <<dist<<" cm."<<endl;
}

void ConsolePrinter::printString(const QString str){
    auto timesrc = std::time(nullptr);
    auto tm = *std::localtime(&timesrc);
    //cout << std::put_time(&tm, "%d-%m-%Y %H-%M-%S")<<": "<< str.toStdString() <<endl;
}
