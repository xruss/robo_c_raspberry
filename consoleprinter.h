#ifndef CONSOLEPRINTER_H
#define CONSOLEPRINTER_H

#include <QObject>
#include <ctime>

class ConsolePrinter : public QObject
{
    Q_OBJECT
public:
    explicit ConsolePrinter(QObject *parent = nullptr);

signals:

public slots:
    void printDistance(double dist);
    void printString(const QString str);
};

#endif // CONSOLEPRINTER_H
