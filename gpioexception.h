#ifndef GPIOEXCEPTION_H
#define GPIOEXCEPTION_H
#include <exception>
#include <QString>

class GpioException : public std::exception
{
public:
    GpioException();
    GpioException(const QString& what);
    const char* what() const  _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT Q_DECL_OVERRIDE {return m_what.data();}
private:
    std::string m_what;
};

#endif // GPIOEXCEPTION_H
