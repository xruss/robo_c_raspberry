#include "gpiomanager.h"
#include "gpioexception.h"
#include <pigpio.h>
#include <iostream>


bool GpioManager::init_done=false;
QMutex GpioManager::mutex(QMutex::Recursive);

GpioManager::GpioManager(qint64 _delay):prevTimer(0),delay(_delay)
{
    QMutexLocker lock(&mutex);
    std::cout<<"init start!"<<std::endl;
    if(!init_done){
        if (gpioInitialise() < 0){ throw GpioException("bad GPIO init call");} else init_done=true;
    }
    std::cout<<"init ok!"<<std::endl;
    timer.start();
}


GpioManager::~GpioManager()
{
    QMutexLocker lock(&mutex);
    if(init_done){
        gpioTerminate();
        init_done=false;
    }
    std::cout <<"deleting GpioManager";
}

int GpioManager::safeGpioServo(unsigned user_gpio, unsigned pulsewidth, uint direction_pin, bool direction){
    QMutexLocker lock(&mutex);
    ensureGpioReady();
    gpioWrite(direction_pin, direction?1:0);
    return gpioServo(user_gpio,pulsewidth);
}


void GpioManager::safeDigitalWrite(uint pin, bool value){
        QMutexLocker lock(&mutex);
        ensureGpioReady();
        gpioWrite(pin, value?1:0);
}
bool GpioManager::safeDigitalRead(uint pin){
        QMutexLocker lock(&mutex);
        ensureGpioReady();
        return gpioRead (pin);
}

void GpioManager::safeMode(uint pin,uint mode){
    QMutexLocker lock(&mutex);
    ensureGpioReady();
    gpioSetMode(pin,mode);
}


void GpioManager::safeTogglePwm(quint32 pin, quint32 dutyCycle){
    QMutexLocker lock(&mutex);
    ensureGpioReady();
    gpioPWM(pin,dutyCycle);
}
void GpioManager::safeSetPwm(quint32 pin,quint32 freq){
    QMutexLocker lock(&mutex);
    ensureGpioReady();
    gpioPWM(pin, freq);
}
void GpioManager::safeSetPwmRange(quint32 pin,quint32 range){
    QMutexLocker lock(&mutex);
    ensureGpioReady();
    gpioSetPWMrange(pin,range);
}



void GpioManager::ensureGpioReady(){
    qint64 newTimer=timer.elapsed();
    while((newTimer-prevTimer) < delay){
        newTimer = timer.elapsed();
    }
    prevTimer=newTimer;
}
