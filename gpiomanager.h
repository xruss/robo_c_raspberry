#ifndef GPIOMANAGER_H
#define GPIOMANAGER_H
#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <QElapsedTimer>

class GpioManager
{

public:
    GpioManager(qint64 _delay=1);
    virtual ~GpioManager();
    int safeGpioServo(unsigned user_gpio, unsigned pulsewidth, uint direction_pin, bool direction);
    void safeDigitalWrite(uint pin, bool value);
    void safeMode(uint pin, uint mode);
    bool safeDigitalRead(uint pin);

   /* gpioPWM                    Start/stop PWM pulses on a GPIO
    gpioSetPWMfrequency        Configure PWM frequency for a GPIO
    gpioSetPWMrange            Configure PWM range for a GPIO*/

    void safeTogglePwm(quint32 pin, quint32 dutyCycle);
    void safeSetPwm(quint32 pin, quint32 freq);
    void safeSetPwmRange(quint32 pin, quint32 range);


constexpr static const uint PI_INPUT = 0;
constexpr static const uint PI_OUTPUT = 1;
constexpr static const uint PI_ALT0= 4;

private:
    void ensureGpioReady();
    static bool init_done;
    static QMutex mutex;
private:

    QElapsedTimer timer;
    qint64 prevTimer;
    qint64 delay;
};

#endif // GPIOMANAGER_H
