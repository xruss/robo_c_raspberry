#include "gyroworker.h"

#include <QSharedPointer>

GyroWorker::GyroWorker(QObject *parent) : QObject(parent),mpu(0x68),x(0),y(0),z(0),yaw(0),pitch(0),roll(0)
{

}



void GyroWorker::doCycle(){
    mpu.getAccel(&x,&y,&z);
    //emit acceleration(x, y, z);
    mpu.getGyro(&roll,&pitch,&yaw);
    //emit angle(yaw,pitch,roll);
    emit data(QSharedPointer<Datagram>(new GyroInfoDatagram(x,y,z,yaw,pitch,roll)));
    emit log(QString("acceleration: x=%1,y=%2,z=%3 angle: yaw=%4,pitch=%5,roll=%6").arg(x).arg(y).arg(z).arg(yaw).arg(pitch).arg(roll));
}
