#ifndef GYROWORKER_H
#define GYROWORKER_H

#include <QObject>
#include <MPU6050.h>
#include <gyroinfodatagram.h>

class GyroWorker : public QObject
{
    Q_OBJECT
public:
    explicit GyroWorker(QObject *parent = nullptr);

signals:
    void data(QSharedPointer<Datagram> datagram);
    void log(const QString str);
public slots:

    void doCycle();
private:
    MPU6050 mpu;
    float x,y,z,yaw,pitch,roll;
};

#endif // GYROWORKER_H
