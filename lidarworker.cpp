#include "lidarworker.h"
#include <distancesdatagram.h>


#define PACKET_SIZE 42
#define HEADER (char)(0xFA)
#define START_ANGLE 0xA0h
#define END_ANGLE 0xDBh

// Нужно сделать распаковку пакета (сериализацию структуры пакета)
// Добавить TimeStamp`ы - время получения данных

LidarWorker::LidarWorker(QObject *parent) : QObject(parent),masterThread(nullptr),buffer(0),printer()
{

}


void LidarWorker::start(){
    report("starting");
    if(!masterThread){

    masterThread= new MasterThread(this);
    connect(masterThread, &MasterThread::response,this,&LidarWorker::response,Qt::QueuedConnection);
    connect(masterThread, &MasterThread::error,this,&LidarWorker::report,Qt::QueuedConnection);
    connect(masterThread, &MasterThread::timeout,this,&LidarWorker::report,Qt::QueuedConnection);
    connect(masterThread, &MasterThread::error,this,&LidarWorker::error,Qt::QueuedConnection);
    connect(masterThread, &MasterThread::timeout,this,&LidarWorker::error,Qt::QueuedConnection);
    connect(masterThread, &MasterThread::log,this,&LidarWorker::report,Qt::QueuedConnection);


    }
    masterThread->transaction(DEFAULT_PORT,DEFAULT_TIMEOUT,START_LIDAR);

}

void LidarWorker::stop(){

    if(masterThread){
        disconnect(masterThread, &MasterThread::response,this,&LidarWorker::response);
        disconnect(masterThread, &MasterThread::error,this,&LidarWorker::report);
        disconnect(masterThread, &MasterThread::timeout,this,&LidarWorker::report);
        disconnect(masterThread, &MasterThread::error,this,&LidarWorker::error);
        disconnect(masterThread, &MasterThread::timeout,this,&LidarWorker::error);
        disconnect(masterThread, &MasterThread::log,this,&LidarWorker::report);

        delete masterThread;
        masterThread= nullptr;
    }
}


LidarWorker::~LidarWorker(){
    stop();
}


void LidarWorker::response(const QByteArray s){

   // report(QString("got data:")+ QString(s.toHex()));
    buffer=buffer+s;
    if(buffer.length()>PACKET_SIZE){
       while((!buffer.startsWith(HEADER))&&(buffer.size()>0))
       {
           buffer=buffer.mid(1);
       }
       if(buffer.length()>PACKET_SIZE){
           QByteArray packet = buffer.left(PACKET_SIZE);
           buffer = buffer.mid(PACKET_SIZE);
           parsePacket(packet);

       }
    }
  //  report("current buffer:"+ QString(buffer.toHex()));
}

void gotData(const QSharedPointer<Datagram> data);



#define START_OFFSET 4
#define ANGLE_DATA_SIZE 6
#define DIST_M_OFFSET 2


void LidarWorker::parsePacket(const QByteArray& packet){
    if(packet.size()!=PACKET_SIZE)
    {
        throw "bad packet size";
    }
    int base = packet[1]&0xFF;
    int angle = (base-0xA0)*6;
    unsigned short rpm = parseShort(packet[2],packet[3]);
    QVector<DistanceData> distances;
    distances.reserve(6);
    emit report("packet: "+ QString(packet.toHex()));
    for (int cur=0;cur<6;cur++){
        DistanceData dd;
        int qual =  START_OFFSET+(ANGLE_DATA_SIZE* cur);
        int dist = qual+DIST_M_OFFSET;
        dd.quality=parseShort(packet[qual],packet[qual+1]);
        dd.distance=parseShort(packet[dist],packet[dist+1]);
        distances.append(dd);
    }
    QSharedPointer<DistancesDatagram> sp(new DistancesDatagram(QDateTime::currentDateTime(),angle,rpm,distances));
    emit gotData(sp);
    emit report(QString("Datagram created: ")+printer.printDatagram(sp));
}


int LidarWorker::parseShort(const unsigned char lo, const unsigned char hi){
    return ((int)lo)+ (((int)hi)<<8);
}


void LidarWorker::error(const QString s){
    report("error: "+ s);
    masterThread->reset();
}
