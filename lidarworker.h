#ifndef LIDARWORKER_H
#define LIDARWORKER_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <datagram.h>
#include <datagramprinter.h>

#include "masterthread.h"

#define DEFAULT_PORT "ttyAMA0"
#define DEFAULT_TIMEOUT 10000
#define START_LIDAR "g"






class LidarWorker : public QObject
{
    Q_OBJECT
public:
    explicit LidarWorker(QObject *parent = nullptr);
    ~LidarWorker();

public slots:
    void start();
    void response(const QByteArray s);
    void error(const QString s);
    void stop();
signals:
    void report(const QString log);
    void gotData(const QSharedPointer<Datagram> data);
private:
    void parsePacket(const QByteArray& packet);
    int parseShort (const unsigned char lo ,const unsigned char hi);



private:
    MasterThread *masterThread;
    QByteArray buffer;
    DatagramPrinter printer;

};

#endif // LIDARWORKER_H
