#include <QCoreApplication>
#include <QTimer>
#include <QTcpServer>
#include <QSerialPortInfo>

#include <iostream>
#include <memory>

#include <datagrambuilder.h>
#include <serverhandler.h>
#include "wheelsconsumer.h"

#include "servoworker.h"
#include "gpiomanager.h"
#include "sonicworker.h"
#include "consoleprinter.h"
#include "gyroworker.h"
#include "wheel.h"
#include "wheelsworker.h"
#include "lidarworker.h"

#include <initializer_list>
#include <signal.h>
#include <unistd.h>


LidarWorker* lidar;
// Исправить CONSUMER
void catchUnixSignals(std::initializer_list<int> quitSignals, void (*handler)(int) ) {


    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    for (auto sig : quitSignals)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    for (auto sig : quitSignals)
        sigaction(sig, &sa, nullptr);
}


int main(int argc, char *argv[])
{ 
    QCoreApplication a(argc, argv);

    qRegisterMetaType<QSharedPointer<Datagram> >();


    QTimer timer;
    QTimer timer2;
    QTimer timer3;
    timer.setInterval(100);
    timer.start();
    timer2.setInterval(10000);
    timer2.start();
    timer3.setInterval(6660);
    timer3.start();

    auto gpio=QSharedPointer<GpioManager>(new GpioManager());
    /*wheels*/
    WheelDesc leftWheelDesc(26/*38,GPIO20*/,13/*33,GPIO13*/,LEFT,200);
    WheelDesc rightWheelDesc(21/*40,GPIO21*/,12/*32,GPIO12*/,RIGHT,200);
    Wheel leftWheel(leftWheelDesc,gpio);
    Wheel rightWheel(rightWheelDesc,gpio);
    QMap<WheelPos,Wheel*> wheels( {std::pair<WheelPos,Wheel*>(LEFT,&leftWheel),std::pair<WheelPos,Wheel*>(RIGHT,&rightWheel)});
    WheelsWorker wheelsWorker(wheels);
    //QObject::connect(&timer2,&QTimer::timeout,&wheelsWorker,&WheelsWorker::defaultForward,Qt::QueuedConnection);
    //QObject::connect(&timer3,&QTimer::timeout,&wheelsWorker,&WheelsWorker::stop,Qt::QueuedConnection);
    /* server*/
    QTcpServer* server = new QTcpServer;
    DatagramBuilder datagramBuilder;
    ServerHandler* handler = new ServerHandler(server,&datagramBuilder);
    handler->setParent(&a);
    WheelsConsumer* wheelsMessageProcessor = new WheelsConsumer(&a);
    QObject::connect(handler,&ServerHandler::newConverter,wheelsMessageProcessor,&WheelsConsumer::onTcpToDatagramsConvertorGot);

   //TODO add wheels  QObject::connect(wheelsMessageProcessor,&WheelsConsumer::forward,&wheelsWorker,&WheelsWorker::forward,Qt::QueuedConnection);
   //TODO add wheels  QObject::connect(wheelsMessageProcessor,&WheelsConsumer::backward,&wheelsWorker,&WheelsWorker::backward,Qt::QueuedConnection);
   //TODO add wheels  QObject::connect(wheelsMessageProcessor,&WheelsConsumer::clockWise,&wheelsWorker,&WheelsWorker::clockWise,Qt::QueuedConnection);
   //TODO add wheels  QObject::connect(wheelsMessageProcessor,&WheelsConsumer::counterClockWise,&wheelsWorker,&WheelsWorker::counterClockWise,Qt::QueuedConnection);
   //TODO add wheels  QObject::connect(wheelsMessageProcessor,&WheelsConsumer::stop,&wheelsWorker,&WheelsWorker::stop,Qt::QueuedConnection);



    QMetaObject::invokeMethod(handler,"start",Qt::QueuedConnection);

    /*servos*/
    QMap<quint32,ServoData> servos;
//    servos[20]=ServoData(100, 1000,true, 32);
//    servos[21]=ServoData(100, 1000,true, 33);
//    std::cout<<servos[20] << "\n" << servos[21];
//    auto gpio=std::make_shared<GpioManager>();
//    servos[27]=ServoData(100, 1000);
//    std::cout<<servos[27];
//    ServoWorker worker(servos,gpio);
//    QObject::connect(&timer,&QTimer::timeout,&worker,&ServoWorker::doCycle,Qt::QueuedConnection);

    SonicWorker sonic(17,4,gpio);
    std::cout<<"sonic ok"<<std::endl;
    ConsolePrinter printer;
    std::cout<<"printer ok"<<std::endl;

    GyroWorker gyro;
   //TODO add gyro  QObject::connect(&timer2,&QTimer::timeout,&gyro,&GyroWorker::doCycle,Qt::QueuedConnection);
    std::cout<<"gyro ok"<<std::endl;
   //TODO add sonic QObject::connect(&sonic,&SonicWorker::distance,&printer,&ConsolePrinter::printDistance,Qt::DirectConnection);
   //TODO add sonic QObject::connect(&sonic,&SonicWorker::log,&printer,&ConsolePrinter::printString,Qt::DirectConnection);
  //  QObject::connect(&worker,&ServoWorker::log,&printer,&ConsolePrinter::printString,Qt::QueuedConnection);
   //TODO add wheels QObject::connect(&wheelsWorker,&WheelsWorker::log,&printer,&ConsolePrinter::printString,Qt::QueuedConnection);
   //TODO add gyro QObject::connect(&gyro,&GyroWorker::log,&printer,&ConsolePrinter::printString,Qt::QueuedConnection);
   //TODO add wheels QObject::connect(wheelsMessageProcessor,&WheelsConsumer::log,&printer,&ConsolePrinter::printString,Qt::QueuedConnection);
   //TODO add gyro QObject::connect(&gyro,&GyroWorker::data,wheelsMessageProcessor,&BasicConsumer::sendDatagram,Qt::QueuedConnection);


    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        printer.printString(QString("Port: %1\n  Location: %2\n  Description: %3\n  Manufacturer: %4\n  Serial number: %5\n")
                            .arg(info.portName()).arg(info.systemLocation()).arg(info.description()).arg(info.manufacturer()).arg(info.serialNumber()));

    lidar = new LidarWorker(&a);


    QObject::connect(lidar,&LidarWorker::report,&printer,&ConsolePrinter::printString,Qt::QueuedConnection);
    QObject::connect(lidar,&LidarWorker::gotData,wheelsMessageProcessor,&BasicConsumer::sendDatagram,Qt::QueuedConnection);

    std::cout<<"before invoke"<<std::endl;
     // QMetaObject::invokeMethod(&sonic, "start", Qt::QueuedConnection);
    QMetaObject::invokeMethod(lidar    , "start", Qt::QueuedConnection);
    std::cout<<"all created"<<std::endl;


    auto sigHandler = [](int sig) -> void {
        // blocking and not aysnc-signal-safe func are valid
        lidar->stop();
        printf("\nquit the application by signal(%d).\n", sig);
        QCoreApplication::quit();
    };


     catchUnixSignals({SIGQUIT, SIGINT, SIGTERM, SIGHUP},sigHandler);

    return a.exec();
}
