#include "serverhandler.h"
#include <QDebug>

ServerHandler::ServerHandler(QTcpServer *server, DatagramBuilder* datagramBuilder, quint16 port):QObject(),m_port(port),m_server(server),m_datagramBuilder(datagramBuilder)
{
    connect(m_server,&QTcpServer::newConnection,this,&ServerHandler::onNewConnection);
    server->setParent(this);
}

void ServerHandler::start(){
    m_server->listen(QHostAddress::Any,m_port);
    qDebug()<<"Server started on port "<<m_port;
}


void ServerHandler::onNewConnection(){
    QTcpSocket* socket =m_server->nextPendingConnection();
    TcpToDatagramsConvertor* convertor = new TcpToDatagramsConvertor(socket,m_datagramBuilder);
    emit newConverter(convertor);

}
