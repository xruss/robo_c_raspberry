#ifndef SERVERHANDLER_H
#define SERVERHANDLER_H

#include <QObject>
#include <QTcpServer>
#include <tcptodatagramsconvertor.h>

class ServerHandler: public QObject
{
    Q_OBJECT
public:
    ServerHandler(QTcpServer* server,DatagramBuilder* datagramBuilder,quint16 port=8080);

public slots:
    void start();
protected slots:
    void onNewConnection();

signals:
    void newConverter(TcpToDatagramsConvertor* newConverter);

private:
    quint16 m_port;
    QTcpServer* m_server;
    DatagramBuilder* m_datagramBuilder;
};

#endif // SERVERHANDLER_H
