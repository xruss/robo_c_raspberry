#include "servoworker.h"
#include "gpioexception.h"
#include <pigpio.h>
#include <iostream>

#define MIN_WIDTH 1000
#define MAX_WIDTH 2000

std::ostream& operator<< (std::ostream& out,const ServoData& data){
    out<<"ServoData: {m_step=\""<<data.m_step<<"\", m_width=\""<<data.m_width<<"\"};";
    return out;
}

ServoWorker::ServoWorker(const QMap<quint32,ServoData> &servos, std::shared_ptr<GpioManager> gpio)
    :QObject(),m_servos(servos),m_rnd(1),m_gpio(gpio)
{

}

ServoWorker::~ServoWorker()
{
    for (quint32 num : m_servos.keys())
    {
          ServoData& data = m_servos[num];
          emit log(QString("servo # %1").arg(num));
          m_gpio->safeGpioServo(num, 0,data.getDirectionPin(),data.getDirection());
    }
  // gpioTerminate();
}


void ServoWorker::randomize(){
    //std::cout<<"randomize begin"<<std::endl;

    emit log("randomize begin");
    for (quint32 num : m_servos.keys())
    {
          emit log(QString("servo # %1").arg(num));
          ServoData& data = m_servos[num];
          std::cout<<num;
          qint32 step =  m_rnd.bounded(5,25);
          data.setDirection( ((step % 2) == 0));
          data.setStep(step);
          data.setWidth( m_rnd.bounded(MIN_WIDTH, MAX_WIDTH));

    }
    emit log("randomize end");
}

void ServoWorker::doCycle(){
  //  emit log("doCycle Begin");
    for (quint32 num : m_servos.keys())
    {
          ServoData& data = m_servos[num];
          emit log(QString("servo # %1").arg(num));

          m_gpio->safeGpioServo(num, data.getWidth(),data.getDirectionPin(),data.getDirection());

           data.setWidth(data.getStep()+data.getWidth());

          if ((data.getWidth()<MIN_WIDTH) || (data.getWidth()>MAX_WIDTH))
          {
              data.setStep(-data.getStep());
              data.setWidth(data.getStep()+data.getWidth());
              data.setDirection(!data.getDirection());
          }
    }
  //  emit log("doCycle end");
}
