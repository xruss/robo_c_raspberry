#ifndef SERVOWORKER_H
#define SERVOWORKER_H

#include <QObject>
#include <QMap>
#include <memory>
#include "gpiomanager.h"


#define NUM_GPIO 32

//servo direction 38 40

//shim 32 33

class ServoData{
public:
  ServoData(qint32 step,quint32 width,bool direction, quint32 direction_pin)
      :m_step(step),m_width(width),m_direction(direction),m_direction_pin(direction_pin){}
  ServoData():m_step(0),m_width(0),m_direction(0),m_direction_pin(0){}

  inline void setStep(qint32 step){
      m_step=step;
  }
  inline void setWidth(quint32 width){
      m_width=width;
  }
  inline void setDirection(bool direction){
      m_direction=direction;
  }
  inline void setDirectionPin(quint32 direction_pin){
      m_direction_pin=direction_pin;
  }


  qint32 getStep()const{return m_step;}
  quint32 getWidth()const{return m_width;}
  bool getDirection()const{return m_direction;}
  quint32 getDirectionPin()const{return m_direction_pin;}

  bool operator==(const ServoData& other)const {
      return (m_step==other.getStep()) && (m_width==other.m_width) && (m_direction == other.m_direction)&& (m_direction_pin == other.m_direction_pin);
  }
  bool operator!=(const ServoData& other)const {
      return !operator==(other);
  }

 friend std::ostream& operator<< (std::ostream& out,const ServoData& data);
private:
  qint32 m_step;
  quint32 m_width;
  bool m_direction;
  quint32 m_direction_pin;

};

std::ostream& operator<< (std::ostream& out,const ServoData& data);

class MyRandomGenerator{
public:
    MyRandomGenerator(const uint seed){
         qsrand(seed);
    }
    quint32 bounded(quint32 from,quint32 to){
         return (static_cast<quint32>(qrand()) % ((to + 1) - from) + from);
    }
};


class ServoWorker: public QObject
{
    Q_OBJECT
public:
    ServoWorker(const QMap<quint32,ServoData> &servos, std::shared_ptr<GpioManager> gpio);
    virtual ~ServoWorker();
    void randomize();
private:
    QMap<quint32,ServoData> m_servos;
    MyRandomGenerator m_rnd;
    std::shared_ptr<GpioManager> m_gpio;
signals:
    void log(QString str);
public slots:

    void doCycle();

};

#endif // SERVOWORKER_H
