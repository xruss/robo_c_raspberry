#include "sonicworker.h"


//#include <iostream>
#include <QThread>
#include <QMutexLocker>
#include <QElapsedTimer>

/*
#Libraries
import RPi.GPIO as GPIO
import time

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    StartTime = time.time()
    StopTime = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()

    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2

    return distance

if __name__ == '__main__':
    try:
        while True:
            dist = distance()
            print ("Measured Distance = %.1f cm" % dist)
            time.sleep(1)

        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
*/



SonicThread::SonicThread(QSharedPointer<GpioManager> gpio, uint gpio_echo, uint gpio_trigger):
QThread(nullptr),m_gpio(gpio),m_echo(gpio_echo),m_trigger(gpio_trigger)
{

}
#define READ_TIMEOUT 1000000
#define MEASURE_TIMEOUT 200000000l
#define SECOND_TIMEOUT 1000000000l

double SonicThread::measureDistance(){
   //set Trigger to HIGH
    QElapsedTimer timer;
    QElapsedTimer watchdog;
    qint64 passed = 0;
  //  emit log("setting trigger...");
    m_gpio->safeDigitalWrite(m_trigger,true);
    //set Trigger after 0.01ms to LOW
    usleep(10);//microsecs
    m_gpio->safeDigitalWrite(m_trigger,false);
  //  emit log("disabling trigger...");
    watchdog.start();
    timer.start();
    int correct = 0;
    //# save StartTime
    while ((!m_gpio->safeDigitalRead(m_echo)) && (correct=(watchdog.elapsed()<READ_TIMEOUT))){
        timer.restart();
    }

   // emit log("reading distance...");
    int r=0;
    //# save time of arrival
    if(correct){
        while ((r=m_gpio->safeDigitalRead(m_echo))&& (passed<MEASURE_TIMEOUT)){
            passed = timer.nsecsElapsed();
           // emit log(QString("signal: %1  passed: %2").arg(r).arg(passed));
        }
    }
    if (passed>MEASURE_TIMEOUT) passed = 0;
 //   emit log("done reading distance...");
    //# time difference between start and arrival
    double elapsed = static_cast<double>(passed)/SECOND_TIMEOUT; //TimeElapsed = StopTime - StartTime
    //# multiply with the sonic speed (34300 cm/s)
    //# and divide by 2, because there and back
    return elapsed * distance_modifier;

}
void SonicThread::run(){
    emit log("running...");
    try{
    while(!isInterruptionRequested()){
       // emit log("calc distance...");
        emit distance(measureDistance());
        msleep(500);
    }
    }catch(...){
        emit log("ERROR!!!");
    }

    emit log("interrupted...");
}


SonicWorker::SonicWorker( uint gpio_echo, uint gpio_trigger, QSharedPointer<GpioManager> gpio)
    :QObject(),m_echo(gpio_echo),m_trigger(gpio_trigger),sonicThread(nullptr),m_gpio(gpio)
{


}

void SonicWorker::start(){
    QMutexLocker lock(&startstop);

    if(!sonicThread){
        emit log("creating thread");
        sonicThread= new SonicThread(m_gpio,m_echo,m_trigger);
        emit log("thread created");
        connect(sonicThread, &SonicThread::distance,this,&SonicWorker::distance,Qt::QueuedConnection);
        connect(sonicThread, &SonicThread::log,this,&SonicWorker::log,Qt::QueuedConnection);
    }

    emit log(QString("isRunning: %1").arg(sonicThread->isRunning()));
    if(!sonicThread->isRunning()){
        emit log("setting pins...");
        m_gpio->safeMode(m_trigger,GpioManager::PI_OUTPUT);
        m_gpio->safeMode(m_echo,GpioManager::PI_INPUT);
        emit log("starting sonic...");
        sonicThread->start();
        emit log("start succeed");
    }
}
void SonicWorker::stop(){
    QMutexLocker lock(&startstop);
    if(sonicThread && sonicThread->isRunning()){
        sonicThread->requestInterruption();
        if(!sonicThread->wait(10000))
            sonicThread->terminate();
        disconnect(sonicThread, &SonicThread::distance,this,&SonicWorker::distance);
        disconnect(sonicThread, &SonicThread::log,this,&SonicWorker::log);
        sonicThread->deleteLater();
        sonicThread=nullptr;
    }
}

SonicWorker::~SonicWorker(){
    stop();
}



