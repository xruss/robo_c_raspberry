#ifndef SONICWORKER_H
#define SONICWORKER_H

#include <QSharedPointer>

#include <QMutex>
#include <QThread>

#include<gpiomanager.h>

class SonicThread:public QThread{
    Q_OBJECT
public:

    SonicThread(QSharedPointer<GpioManager> gpio, uint gpio_echo, uint gpio_trigger);


    constexpr const static double distance_modifier = 17150.0;
signals:
    void distance(double dist);
    void log(const QString data);

private:
    double measureDistance();

    QSharedPointer<GpioManager> m_gpio;
    uint m_echo;
    uint m_trigger;
public:
    void run() Q_DECL_OVERRIDE;
};

class SonicWorker : public QObject
{
    friend class SonicThread;

    Q_OBJECT
public:
    SonicWorker(uint gpio_echo, uint gpio_trigger, QSharedPointer<GpioManager> gpio);
    ~SonicWorker();
public slots:
    void start();
    void stop();
private:
    uint m_echo;
    uint m_trigger;
    SonicThread* sonicThread;
    QSharedPointer<GpioManager> m_gpio;
    QMutex startstop;
signals:
    void distance(double dist);
    void log(const QString data);

};

#endif // SONICWORKER_H
