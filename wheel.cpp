
#include "wheel.h"

/*
gpioPWM                    Start/stop PWM pulses on a GPIO
gpioSetPWMfrequency        Configure PWM frequency for a GPIO
gpioSetPWMrange            Configure PWM range for a GPIO*/

Wheel::Wheel(WheelDesc _desc, QSharedPointer<GpioManager> _manager, bool _direction, quint32 _speed)
    :description(_desc),manager(_manager),direction(_direction), speed(_speed)
{
    initMotor();
   // applyToMotor();
}



void Wheel::initMotor(){


    manager->safeMode(description.directionPin,GpioManager::PI_OUTPUT);
    manager->safeMode(description.speedPin,GpioManager::PI_OUTPUT);
  //  manager->safeTogglePwm(description.speedPin,  1024  );
  //  manager->safeSetPwmFrequency(description.speedPin, 10000);
  //  manager->safeSetPwmRange(description.speedPin,description.maxSpeed);
  //  manager->safeTogglePwm(description.speedPin,0);
}

void Wheel::applyToMotor(){
    manager->safeDigitalWrite(description.directionPin,direction);
    manager->safeSetPwm(description.speedPin,speed);
}
