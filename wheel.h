#ifndef WHEEL_H
#define WHEEL_H

#include <QtGlobal>
#include "gpiomanager.h"
#include <QSharedPointer>

enum WheelPos{
    LEFT,
    RIGHT
};

struct WheelDesc
{
    WheelDesc(quint32 _directionPin, quint32 _speedPin, WheelPos _position,quint32 _maxSpeed)
        :directionPin(_directionPin),speedPin(_speedPin), position(_position), maxSpeed(_maxSpeed){}
    quint32 directionPin;
    quint32 speedPin;
    WheelPos position;
    quint32 maxSpeed;
};

class Wheel{

public:
    Wheel(WheelDesc _desc, QSharedPointer<GpioManager> _manager, bool _direction = Wheel::FORWARD, quint32 _speed=0);
    inline const WheelDesc& getDescription()const {return description;}

    inline  bool getDirection()const {return direction;}
    inline  quint32 getSpeed()const {return speed;}

    inline void setDirection(bool newDirection){direction=newDirection; applyToMotor();}
    inline void setSpeed(quint32 newSpeed){speed=newSpeed; applyToMotor(); }
    inline void reset(bool newDirection,quint32 newSpeed){direction=newDirection;speed=newSpeed; applyToMotor();}

    static constexpr const bool FORWARD = true;
    static constexpr const bool BACKWARD = false;

private:

    void initMotor();
    void applyToMotor();

    const WheelDesc description;
    QSharedPointer<GpioManager> manager;
    bool direction;
    quint32 speed;
    //bool activated;
};


#endif // WHEEL_H
