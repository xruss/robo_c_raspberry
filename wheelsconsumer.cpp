#include "wheelsconsumer.h"
#include <movementdatagram.h>

WheelsConsumer::WheelsConsumer(QObject *parent):BasicConsumer (parent)
{
    connect(this,&BasicConsumer::logData,this,&WheelsConsumer::log);
}

const QMap<QString,WheelsConsumer::KNOWN_DATAGRAM_TYPE> WheelsConsumer::knownDatagrams = {
        std::pair<QString,WheelsConsumer::KNOWN_DATAGRAM_TYPE>(MovementDatagram::name,WheelsConsumer::MOVEMENT_DATAGRAM)
     };

void WheelsConsumer::doDatagram(QSharedPointer<Datagram> datagram)
{
    auto type =datagram->getType();
    emit log("new datagram: "+type);
        switch(knownDatagrams.value(type,WheelsConsumer::UNKNOWN)){
            case MOVEMENT_DATAGRAM:
                onMovementDatagram(datagram.data());
                break;
            case UNKNOWN:
                break;
        };
}


void WheelsConsumer::onMovementDatagram(Datagram* datagram){
    MovementDatagram* md = dynamic_cast<MovementDatagram*>(datagram);
    quint32 speed = static_cast<quint32>(md->getSpeed());
    emit log(QString("speed: %1").arg(md->getSpeed()));
    if (speed>0){
        switch (md->getDirection()){
        case FORWARD:{
            emit forward(speed);
            break;
        }
        case BACKWARD:{
            emit backward(speed);
            break;
        }
        case LEFT:{
            emit counterClockWise(speed);
            break;
        }
        case RIGHT:{
            emit clockWise(speed);
            break;
        }
        }
    }else{
        emit stop();
    }

}



