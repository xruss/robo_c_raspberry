#ifndef WHEELSCONSUMER_H
#define WHEELSCONSUMER_H

#include <QObject>
#include <QMap>
#include <basicconsumer.h>

class WheelsConsumer : public BasicConsumer
{
    Q_OBJECT
public:
    WheelsConsumer(QObject* parent = nullptr);

protected:
    virtual void doDatagram(QSharedPointer<Datagram> datagram);
private:
    enum KNOWN_DATAGRAM_TYPE{
        UNKNOWN = 0,
        MOVEMENT_DATAGRAM
    };

const static QMap<QString,WheelsConsumer::KNOWN_DATAGRAM_TYPE> knownDatagrams;
//MovementDatagram::name
    void onMovementDatagram(Datagram* datagram);

signals:
    void forward(quint32 speed);
    void backward(quint32 speed);
    void counterClockWise(quint32 speed);
    void clockWise(quint32 speed);
    void stop();
    void log(const QString& data);

};

#endif // WHEELSCONSUMER_H
