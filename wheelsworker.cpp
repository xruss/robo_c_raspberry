#include "wheelsworker.h"



WheelsWorker::WheelsWorker(const QMap<WheelPos,Wheel*> &servos, QObject *parent): QObject(parent), m_servos(servos){


}

void WheelsWorker::forward(quint32 speed){
    emit log(QString("forward at %1").arg(speed));
    m_servos[LEFT]->reset(Wheel::FORWARD, speed);
    m_servos[RIGHT]->reset(Wheel::BACKWARD, speed);
}
void WheelsWorker::backward(quint32 speed){
    emit log(QString("backward at %1").arg(speed));
    m_servos[LEFT]->reset(Wheel::BACKWARD, speed);
    m_servos[RIGHT]->reset(Wheel::FORWARD, speed);
}
void WheelsWorker::counterClockWise(quint32 speed){
    emit log(QString("counterClockWise at %1").arg(speed));
    m_servos[LEFT]->reset(Wheel::BACKWARD, speed);
    m_servos[RIGHT]->reset(Wheel::BACKWARD, speed);
}
void WheelsWorker::clockWise(quint32 speed){
    emit log(QString("clockWise at %1").arg(speed));
    m_servos[LEFT]->reset(Wheel::FORWARD, speed);
    m_servos[RIGHT]->reset(Wheel::FORWARD, speed);
}
void WheelsWorker::stop(){
    emit log(QString("stop"));
    m_servos[LEFT]->reset(Wheel::FORWARD, 0);
    m_servos[RIGHT]->reset(Wheel::BACKWARD, 0);
}

WheelsWorker::~WheelsWorker(){
    stop();
}

/*
private:
QMap<WheelPos,Wheel> m_servos;*/
