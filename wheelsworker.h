#ifndef WHEELSWORKER_H
#define WHEELSWORKER_H

#include <QObject>
#include <QMap>
#include "wheel.h"

class WheelsWorker : public QObject
{
    Q_OBJECT
public:
   // explicit WheelsWorker(QObject *parent = nullptr);
    explicit WheelsWorker(const QMap<WheelPos,Wheel*> &servos, QObject *parent = nullptr);
    virtual ~WheelsWorker();

signals:
    void log (const QString logStr);
public slots:
    void defaultForward(){forward(200);}
    void forward(quint32 speed);
    void backward(quint32 speed);
    void counterClockWise(quint32 speed);
    void clockWise(quint32 speed);
    void stop();
private:
    QMap<WheelPos,Wheel*> m_servos;

};

#endif // WHEELSWORKER_H
